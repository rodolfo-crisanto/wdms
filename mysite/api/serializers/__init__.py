from .util_serializers import NoneSerializer

from .company_serializers import (
    CompanySerializer, CompanyCreateSerializer,
    CompanyEditSerializer, CompanyActionSerializer)

from .area_serializers import (
    AreaSerializer, AreaCreateSerializer,
    AreaEditSerializer, AreaActionSerializer, AreaImportSerializer)

from .department_serializers import (
    DepartmentSerializer, DepartmentCreateSerializer,
    DepartmentEditSerializer, DepartmentActionSerializer, DepartmentImportSerializer)

from .device_serializers import (
    DeviceSerializer, DeviceCreateSerializer,
    DeviceEditSerializer, DeviceActionSerializer,
    DeviceAdjustAreaSerializer, DeviceSendsmsSerializer,
    DeviceExportSerializer)

from .workcode_serializers import (
    WorkcodeSerializer, WorkcodeCreateSerializer,
    WorkcodeEditSerializer, WorkcodeActionSerializer)

from .message_serializers import (
    MessageSerializer, MessageCreateSerializer,
    MessageEditSerializer, MessageActionSerializer)

from .employee_serializers import (
    EmployeeSerializer, EmployeeCreateSerializer,
    EmployeeEditSerializer, EmployeeActionSerializer,
    EmployeeAdjustAreaSerializer, EmployeeAdjustDepartmentSerializer,
    EmployeeImportSerializer, EmployeeSendsmsSerializer,
    EmployeeExportSerializer)

from .template_serializers import (
    FingerTemplateSerializer, FaceTemplateSerializer,
    BioTemplateSerializer)

from .transaction_serializers import (
    TransactionSerializer, TransactionActionSerializer,
    TransactionDeleteAttPhotoSerializer,
    TransactionUsbDiskImportSerializer)

from .adminlog_serializers import AdminLogSerializer, AdminLogActionSerializer

from .group_serializers import GroupSerializer, GroupActionSerializer

from .permission_serializers import PermissionSerializer

from .user_serializers import (
    UserSerializer, UserCreateSerializer,
    UserEditSerializer, UserActionSerializer,
    UserChangePasswordSerializer)

from .license_serializers import (LicenseSerializer,
    LicenseActivationSerializer, LicenseOfflineActivationSerializer)

# from .area_serializers import (
#     AreaSerializer, AreaCreateSerializer,
#     AreaEditSerializer, AreaActionSerializer)

# from .area_serializers import (
#     AreaSerializer, AreaCreateSerializer,
#     AreaEditSerializer, AreaActionSerializer)
