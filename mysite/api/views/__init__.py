from mysite.api.views.view_api_area import AreaViewSet
from mysite.api.views.view_api_company import CompanyViewSet
from mysite.api.views.view_api_department import DepartmentViewSet
from mysite.api.views.view_api_employee import EmployeeViewSet
from mysite.api.views.view_api_transaction import TransactionViewSet
from mysite.api.views.view_api_template import FingerTemplateViewSet, FaceTemplateViewSet, BioTemplateViewSet
from mysite.api.views.view_api_device import DeviceViewSet
from mysite.api.views.view_api_workcode import WorkcodeViewSet
from mysite.api.views.view_api_message import MessageViewSet
from mysite.api.views.view_api_changelog import ChangeLogViewSet
from mysite.api.views.view_api_adminlog import AdminLogViewSet
from mysite.api.views.view_api_group import GroupViewSet
from mysite.api.views.view_api_permission import PermissionViewSet
from mysite.api.views.view_api_user import UserViewSet
from mysite.api.views.view_api_license import LicenseViewSet
from mysite.api.views.view_api_monitor import MonitorViewSet
from mysite.api.views.view_api_redis import RedisViewSet
from mysite.api.views.view_api_migrate import MigrationViewSet
