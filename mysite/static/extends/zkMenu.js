/**
 * Created by Arvin on 2018/9/10.
 */
layui.define(['element'], function (exports) {
  var element = layui.element;
  var ZKMenu = function () {
    this.config = {
      data: [],
      pin_html: $.fn.adminSite.defaults.pined_tab,
    }
  };
  ZKMenu.prototype.render = function (opts) {
    var _menu = this;
    $.extend(true, _menu.config, opts);
    _menu.showMenu();
  };
  ZKMenu.prototype.loadMenu = function () {

  };
  ZKMenu.prototype.showMenu = function () {
    var _menu = this;
    var data = _menu.config.data;
    $.each(data, function (i, v) {
      // App Menu
      var _app_name = v['app'],
        app = $(interject(
          '<li class="layui-nav-item"><div class="menu_module"><a href="javascript:void(0);" app="{app_name}">{app_label}</a></div></li>', {
            'app_name': v['app'],
            'app_label': v['label']
          }, true));

      $("#top_module_menu_nav").append(app);
      // App Group
      if (v['groups'] && v['groups'].length > 0) {
        $.each(v['groups'], function (i, g) {
          // console.warn('#group:', g);
          var group = interject(
            '<li class="layui-nav-item side-menu-group" app="{app_name}" title="{group_label}"><a href="javascript:void(0);"><i class="menu-icon fa fa-fw {menu_icon}"></i><span>{group_label}</span></a>', {
              'app_name': _app_name,
              'group_label': g['label'],
              //'group_label': titleCase(g['label'], '_'),
              'menu_icon': g['icon'].length ? g['icon'] : 'fa-th-list',
            }, true);

          // Menu Items
          if (g['menus'] && g['menus'].length > 0) {
            group += '<dl class="layui-nav-child">';
            $.each(g['menus'], function (i, m) {
              var is_pin = m['pin'];
              group += interject('<dd class=""><a href="javascript:void(0);" hex="{m_hex}" link="{m_url}" title="{m_label}" class="{pin}"><i class="layui-icon">{m_pin}</i><span>{m_label}</span></a></dd>', {
                'm_hex': m['hex'],
                'm_url': m['url'],
                'pin': is_pin ? ' pined-tab-item' : '',
                'm_pin': is_pin ? _menu.config.pin_html : '',
                'm_label': titleCase(m['label'], '_'),
              }, true);

            });
            group += '</d1>'
          }
          group += '</li>';
          $("#side_menu_nav").append(group);
        })
      }
    });
    element.render("nav");
    $('#side_menu_nav > li.layui-nav-item.side-menu-group > a').on('click', function (e) {
      e.stopPropagation();
      var elem = $(this),
        parent = elem.closest('li.layui-nav-item.side-menu-group');
        parent.siblings('.layui-nav-itemed').removeClass('layui-nav-itemed');
    });
    // $('#side_menu_nav dl').on('click', 'dd', function (e) {
    //   e.stopPropagation();
    //   var elem = $(e.target);
    //   elem.find('a').trigger('click');
    // });

    //   .mouseover(function () {
    //   var elem = $(this),
    //   is_collapsed = elem.closest('.layui-side-collapsed').length > 0;
    //   console.log('is_collapsed', is_collapsed);
    // });
    var popup_menu_item = '<li class="side-menu-item" role="menuitem"><a href="javascript:void(0);" hex="{m_hex}" link="{m_url}" tab="true"><span>{m_label}</span></a></li>';

  };
  var zkMenu = new ZKMenu();
  exports('zkMenu', zkMenu)
});
