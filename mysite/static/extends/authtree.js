/*
 * @Author: Boyce Gao
 * @Date: 2018-09-02 18:13:47
 * @Last Modified by:  Boyce Gao
 * @Last Modified time: 2018-09-04 17:02:28
 */

/* JavaScript equivalent to python Format */
if (!String.format) {
  String.format = function(format) {
    var args = Array.prototype.slice.call(arguments, 1);
    return format.replace(/{(\d+)}/g, function(match, number) {
      return typeof args[number] !== 'undefined' ?
        args[number] :
        match;
    });
  };
}

// 节点树
layui.define(['jquery', 'form'], function(exports) {
  // console.log('### exports:', exports);
  function pre_process_selector(dst) {
    if (typeof dst === 'string') {
      dst = [dst,];
    }

    if (dst instanceof Array) {
      return dst;
    } else {
      return [];
    }
  }

  function find_result(dst, condition) {
    dst = pre_process_selector(dst);
    var data = [];

    dst.forEach(function (selector/*, index*/) {
      $(selector).find(condition).each(function(index, item) {
        data.push(item.value);
      });
    });
    return data;
  }

  var $ = layui.jquery,
    form = layui.form,
    export_interface = {
      // 渲染 + 绑定事件
      /**
       * 渲染DOM并绑定事件
       * @param  {[type]} dst       [目标ID，如：#test1]
       * @param  {[type]} trees     [数据，格式：{}] 请求得到的 json 数据
       * @param  {[type]} opt
       *   @param  {[type]} opt.inputname [上传表单名]
       *   @param  {[type]} opt.layfilter [lay-filter的值]
       *   @param  {[type]} opt.openall [默认展开全部]
       * @return {[type]}           [description]
       */
      render: function(dst, trees, opt) {
        var inputname = opt.inputname ? opt.inputname : 'menuids[]';
        var layfilter = opt.layfilter ? opt.layfilter : 'checkauth';
        var openall = opt.openall ? opt.openall : false;
        $(dst).html(export_interface.renderAuth(trees, 0, { inputname: inputname, layfilter: layfilter, openall: openall }).string);
        form.render();

        // 备注：如果使用form.on('checkbox()')，外部就无法使用form.on()监听同样的元素了（LAYUI不支持重复监听了）。
        // form.on('checkbox('+layfilter+')', function(data){
        //  /*属下所有权限状态跟随，如果选中，往上走全部选中*/
        //  var childs = $(data.elem).parent().next().find('input[type="checkbox"]').prop('checked', data.elem.checked);
        //  if(data.elem.checked){
        //    /*查找child的前边一个元素，并将里边的checkbox选中状态改为true。*/
        //    $(data.elem).parents('.auth-child').prev().find('input[type="checkbox"]').prop('checked', true);
        //  }
        //  form.render('checkbox');
        // });

        $(dst).find('.auth-unity:first').unbind('click').on('click', '.layui-form-checkbox', function() {
          var click_elem = $(this);
          // console.log('click_elem', click_elem.context.textContent);

          // var div = document.createElement("div");
          // div.innerHTML = click_elem.context.outerHTML;
          // var parent_text = div.textContent || div.innerText || "";
          var hide_input_elem = click_elem.prev();
          var checked = hide_input_elem.is(':checked');
          var changed_parent = [];

          /* [V] 父结点选中时，全部子结点自动选中 */
          var childs = hide_input_elem.parent('.auth-status').next('.auth-child').find('input[type="checkbox"]').prop('checked', checked);
          /* [V] 仅当下面的所有子结点都已经选中之后，父结点才自动选中 */
          var all_parent = hide_input_elem.parents('.auth-child');
          /* DONE: 应当区分全选和部分勾选 */
          // console.log('all_parent', all_parent);

          all_parent.each(function(_index, elem) {
            var that = $(elem);
            var cur_auth_status = that.prev().children('input[type="checkbox"]');

            /*
             *  1. 如果是点击的元素是要进行**勾选操作**的话，判断下面的子结点是否全部勾选，
             *  * 是的话，令上级一并勾选;
             *  * 否则令上级处于部分勾选状态
             *  2. 如果是点击的元素是要进行**取消勾选操作**的话，判断下面的子结点是否全部未勾选是的话，
             *  * 是则令上级处于部分勾选状态
             *  * 否则令上级处于未勾选状态
             */
            var flag = checked;
            var all_sub_checkbox = that.find('.auth-unity > div > .auth-status > input[type=checkbox]');
            for (var i = 0, n = all_sub_checkbox.length; i < n; i++) {
              var $elem = $(all_sub_checkbox[i]);
              if ($elem.prop('checked') !== checked) {
                flag = !flag;
                break;
              }
            }
            // console.log('flag', flag, 'checked', checked);
            // TT || FF | FT
            if (checked && flag || !checked && !flag || flag && !checked) {
              cur_auth_status.prop('checked', checked);
            // TF || FT
            }
            if (!flag && checked || flag && !checked) {
              /* 此路不通，此处修改后再 render, 修改内容将会丢失, 因为 layui 的checkbox DOM 是重新生成的 */
              // layui_checkbox = cur_auth_status.siblings('.layui-form-checkbox[lay-skin=primary]');
              // console.log('### layui_check_box', layui_checkbox);
              // if (!layui_checkbox.is('.layui-form-checked')) {
              //   layui_checkbox.addClass('layui-form-checked-part');
              // }
              var $input_tag = that.prev().find('input[lay-filter=lay-check-auth]');
              var title = $input_tag.prop('title');
              var value = $input_tag.prop('value');
              changed_parent.push({
                'title': title,
                'value': value,
              });
            }
            // console.log('check_box:', cur_auth_status);
          });

          form.render('checkbox');
          layui.each(changed_parent, function(index, item) {
            // var parent = $(String.format('input[lay-filter=lay-check-auth][title={0}][value={1}]', item.title, item.value));
            var parent = $(String.format('input[lay-filter=lay-check-auth][value={0}]', item.value));

            var layui_checkbox = parent.siblings('.layui-form-checkbox[lay-skin=primary]');
            if (layui_checkbox.is('.layui-form-checked')) {
              layui_checkbox.removeClass('layui-form-checked');
            }
            layui_checkbox.addClass('layui-form-checked-part');
          });
        });

        function fold_event_handler(pseudo_class_scope) {
          // console.log('### fold_event_handler', pseudo_class_scope);
          if (pseudo_class_scope === undefined) {
            pseudo_class_scope = '';
          } else {
            pseudo_class_scope = ':' + pseudo_class_scope;
          }

          return function(event) {
            event.preventDefault();
            var origin = $(this);
            // console.log('origin', origin);
            var child = origin.parent().parent().find('.auth-child' + pseudo_class_scope);
            // console.log('child', child);

            if (origin.is('.active')) { /* 收起 */
              origin.removeClass('active layui-icon-triangle-d').addClass('layui-icon-triangle-r');
              child.each(function (index, elem) {
                $(elem).prev().children('i.auth-icon').removeClass('active layui-icon-triangle-d').addClass('layui-icon-triangle-r');
              });
              child.slideUp('fast');
              return false;
            } else { /* 展开 */
              origin.removeClass('layui-icon-triangle-r').addClass('active layui-icon-triangle-d');
              child.each(function (index, elem) {
                $(elem).prev().children('i.auth-icon').removeClass('layui-icon-triangle-r').addClass('active layui-icon-triangle-d');
              });
              child.slideDown('fast');
              return true;
            }
          }
        }

        /* 动态绑定展开和折叠事件 */
        $(dst)
          .unbind('click').on('click', '.auth-icon', fold_event_handler('first')) /* 绑定双击事件 */
          .unbind('contextmenu').on('contextmenu', '.auth-icon', fold_event_handler()); /* 绑定双击事件 */
      },

      /**
       * @author: boyce
       * @description: 递归创建格式 - 渲染权限树
       * @param  {[json]} tree -- 待渲染的树
       * @param  {[int]} dept -- 渲染的深度
       * @param  {[object]} opt -- 渲染的选项
       * @return {string} 渲染结果
       */
      renderAuth: function(tree, dept, opt) {
        var inputname = opt.inputname;
        var layfilter = opt.layfilter;
        var openall = opt.openall;
        // var str = '<div class="auth-unity">';
        var str_arr = [],
          child_cnt = 0,
          check_cnt = 0;
        str_arr.push('<div class="auth-unity">');


        layui.each(tree, function(index, item) {
          // item.subtree 如有存在，则需要递归处理
          child_cnt += 1;
          var hasChild = item.subtree,
            openChild = item.open,
          // 注意：首递归递进行归调用时，this的环境会改变！否则将无限递归
            sub_tree = export_interface.renderAuth(item.subtree, dept + 1, opt),
            append = hasChild ? sub_tree.string : '';

          var sub_child_cnt = sub_tree.checkCount,
            sub_check_cnt = sub_tree.childCount,
            all_child_checked = sub_child_cnt >0 && sub_child_cnt === sub_check_cnt;



          // '+new Array(dept * 4).join('&nbsp;')+'
          str_arr.push('<div><div class="auth-status"> ');

          // check-box 前面的指示器
          var indicator = '<i class="layui-icon {0}" style="{1}"></i>';
          if (hasChild) {
            // 添加可点击的图标和样式效果
            str_arr.push(String.format(
              indicator, (openall || openChild ? 'auth-icon layui-icon-triangle-d active' : 'auth-icon layui-icon-triangle-r'),
              "cursor:pointer;"));
          } else {
            str_arr.push(String.format(indicator, "auth-leaf layui-icon-circle", "opacity:0;"));
          }

          if (item.checked) {
            check_cnt += 1;
          }

          str_arr.push(
            interpolate(
              '%(dept)s<input type="checkbox" name="%(name)s" title="%(title)s" value="%(value)s" lay-skin="primary" lay-filter="%(filter)s" %(check)s> </div> <div class="auth-child" style="%(style)spadding-left:40px;">', {
                'dept': (dept > 0 ? '<span>├─ </span>' : ''),
                'name': inputname,
                'title': item["name"],
                'value': item["value"],
                'filter': layfilter,
                'check': (item["checked"] || all_child_checked ? 'checked="checked"' : ''),
                'style': (openall || openChild ? '' : 'display:none;'),
              }
            )
          );
          /*
          String.format('{0}<input type="checkbox" name="{1}" title="{2}" value="{3}" lay-skin="primary" lay-filter="{4}" {5}> </div> <div class="auth-child" style="{6}padding-left:40px;">',
            (dept > 0 ? '<span>├─ </span>' : ''), inputname, item.name, item.value, layfilter,
            (item.checked ? 'checked="checked"' : ''), (openall || openChild ? '' : 'display:none;')
          )
          */

          str_arr.push(append);
          str_arr.push('</div></div>')

          // str += '<div><div class="auth-status"> ' +
          //   (hasChild ?
          //     '<i class="layui-icon auth-icon ' + (openall ? 'active' : '') + '" style="cursor:pointer;">' +
          //     (openall ? '&#xe625;' : '&#xe623;') + '</i>' :
          //     '<i class="layui-icon auth-leaf" style="opacity:0;">&#xe626;</i>'
          //   ) +
          //   (dept > 0 ? '<span>├─ </span>' : '') + '<input type="checkbox" name="' + inputname +
          //   '" title="' + item.name + '" value="' + item.value + '" lay-skin="primary" lay-filter="' + layfilter + '" ' +
          //   (item.checked ? 'checked="checked"' : '') + '> </div> <div class="auth-child" style="' +
          //   (openall ? '' : 'display:none;') + 'padding-left:40px;">' + append + '</div></div>';
        });
        str_arr.push('</div>');
        // str += '</div>';

        // return str_arr.join('');
        // console.log(str)
        // console.log(str_arr.join(''), str === str_arr.join(''));
        return {
         "string": str_arr.join(''),
          "childCount": child_cnt,
          "checkCount": check_cnt,
        };
      },
      // 获取选中叶子结点
      getLeaf: function(dst) {
        dst = pre_process_selector(dst);
        var data = [];
        dst.forEach(function (selector/*, index*/) {
          var leafs = $(selector).find('.auth-leaf').parent().find('input[type="checkbox"]:checked');
          leafs.each(function(index, item) {
            data.push(item.value);
          });
        });
        return data;
      },
      // 获取所有的数据
      getAll: function(dst) {
        return find_result(dst, 'input[type="checkbox"]');
      },
      // 获取所有选中的数据
      getChecked: function(dst) {
        return find_result(dst, 'input[type="checkbox"]:checked');
      },
      // 获取未选中数据
      getNotChecked: function(dst) {
        return find_result(dst, 'input[type="checkbox"]:not(:checked)')
      }
    };
  // call the callback function: set_app, set up config status
  exports('authtree', export_interface);
});
