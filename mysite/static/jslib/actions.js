"use strict";

// noinspection ES6ModulesDependencies
(function ($) {
    var last_hidden_fields = [];
    $.fn.actions = function (options) {
      var opts = $.extend({}, $.fn.actions.defaults, options);

      ////Reload currently page
      var reload_page = function reload_page(extra_options) {
        if (extra_options === undefined) {
          extra_options = {};
        }
        var options = {page: {curr: 1}};
        layui.table.reload(opts.tableID, $.extend(true, options, extra_options));
        actionTemplate();
      }, default_callback_action = reload_page;

      // Action post handler
      var action_post = function action_post(op, index, layero) {
        var valid_form = window["before_submit_" + opts.tableId];
        if (valid_form && $.isFunction(valid_form) && !valid_form()) {
          return;
        }
        var post_url = op.url;
        var data_form = $(layero).find("form.layui-form");
        var post_data = {};
        var file_flag = false
        if (data_form.length === 0) {
          var object_ids = new Array();
          op.objects.forEach(function (item){
            object_ids.push(item.id)
          });

          post_data['object_ids'] = object_ids;
          post_data['action_type'] = op.name;
        } else{
          data_form = new FormData(data_form[0]);
          data_form.forEach((value, key) => post_data[key] = value);
          if (op.post_url){
            post_url = op.post_url
          }

          if (op.batch_select === true) {
            var object_ids = new Array();
            op.objects.forEach(function (item){
              object_ids.push(item.id)
            });

            post_data['object_ids'] = object_ids;
            post_data['action_type'] = op.name;
          }

          // 判断提交的表单是否有文件，如果有文件则提交方式要改变
          data_form.forEach( function(value, key) {
            if (value.toString() === '[object File]') {
              file_flag = true
            }
          });
        }

        // 为请求添加url参数
        if(op.query_params_flag == true) {
          if(opts.model === 'transaction') {
            var extra_params = {'delete_type': 'delete'}
            $.extend(extra_params, get_layui_table_config('transaction').where)
            post_url = concatenate_url(post_url, extra_params)
          }
        }

        console.log(post_data)

        if(file_flag === false) {
          $.ajax({
            url: post_url,
            type: "POST",
            data: JSON.stringify(post_data),
            dataType: "json",
            headers: {'content-type':'application/json'},
            success: function(response, status, xhr){
              console.log(response);
              layer.close(index);
              layer.closeAll('loading');
              layer.msg('Success', {time: 1000, icon: 1});
              //需要刷新页面
              reload_page(opts)
            },
            error: function (response) {
              layer.closeAll('loading');
              // alert(response.responseText);
              var res_msg = JSON.parse(response.responseText)
              var display_msg = ''
              for (var prop in res_msg) {
                display_msg += '<p>' + prop + ": " + res_msg[prop] + '</p>'
                // layer.tips(res_msg[prop], 'form input[name="' + prop + '"]', {
                //   tips: [2, '#3595CC'],
                //   tipsMore: true,
                //   closeBtn: 1,
                //   time: 0
                // });
                // layer.tips(res_msg[prop], 'form [id="form-item-' + prop + '"]', {
                //   tips: [2, '#3595CC'],
                //   tipsMore: true,
                //   closeBtn: 1,
                //   time: 0
                // });
                $('form input[name="' + prop + '"]').addClass('layui-form-danger');
                $('form select[name="' + prop + '"]').addClass('layui-form-danger');
              }
              layer.alert(display_msg, {'icon': 0, btn: ['ok'], title: 'message'})
              console.log(response.responseText);
              // layer.msg(response.responseText, {time: 3000, icon: 5});
            }
          });
        } else {
          $.ajax({
            url: post_url,
            type: 'POST',
            data: data_form,
            contentType: false,
            processData: false,
            success: function(response, status, xhr){
              console.log(response);
              layer.close(index);
              layer.closeAll('loading');
              layer.msg('Success', {time: 1000, icon: 1});
              //需要刷新页面
              reload_page(opts)
            },
            error: function (response) {
              layer.closeAll('loading');
              var res_msg = JSON.parse(response.responseText)
              var display_msg = ''
              for (var prop in res_msg) {
                display_msg += '<p>' + prop + ": " + res_msg[prop] + '</p>'
                $('form input[name="' + prop + '"]').addClass('layui-form-danger');
                $('form select[name="' + prop + '"]').addClass('layui-form-danger');
              }
              layer.alert(display_msg, {'icon': 0, btn: ['ok'], title: 'message'})
              console.log(response.responseText);
              // layer.msg(response.responseText, {time: 3000, icon: 5});
            }
          });
        }
      };

      // Action page render
      var action_render = function action_render(op, html) {
        layui.use(["layer", "form"], function () {
          var $ = layui.jquery,
            layer = layui.layer,
            form = layui.form;

          layer.open({
            type: 1,
            zIndex: 998,
            area: [opts.dimension.width, opts.dimension.height],
            content: html,
            title: op.verbose_name,
            btn: ["Submit", "Close"],
            success: function success(layero) {
              var c = $(layero).find("#additional-form-data");
              $(c).append('<input name="action_name" value="' + op.name + '"/>');
              if (op.objects) {
                op.objects.forEach(function (item/*, _idx*/) {
                  $(c).append('<input name="id" value="' + item.id + '"/>');
                });
              }
              form.render();
            },
            yes: function yes(index, layero) {
              action_post(op, index, layero);
            },
            end: function end() {
              layer.closeAll("tips");
            }
          });
        });
      };
      // Action Request Handler
      var action_request = function action_request(params) {
        var data = {
          action_name: params.action.name,
          _popup: true
        };
        $.ajax({
          url: params.action.url,
          type: "GET",
          dataType: "html",
          data: data,
          success: function success(content) {
            action_render(params.action, content);
          }
        });
        var valid_form = window["before_submit_" + opts.tableId];
        if (valid_form && $.isFunction(valid_form) && !valid_form()) {
          return;
        }
      };

      var get_where = function get_where() {
        var search_inputs = {};
        $(opts.container)
          .find("div.search-form input,select")
          .each(function () {
            var val = $(this).val();
            var f = $(this).attr("name");
            if (f && val) {
              search_inputs[f] = val;
            }
          });
        return search_inputs;
      };

      var actionTemplate = function actionTemplate() {
        //Export columns filter
        var action_bar_east = {
            filter: {
              title: "Columns",
              layEvent: opts.model + "_columns",
              icon: "layui-icon-cols"
            }
          }
        if (opts['export']) {
          action_bar_east['exports'] = {
              title: "Export",
              layEvent: opts.model + "_export",
              icon: "layui-icon-export"
            }
        }
        // debugger
        var _temp_container = [];
        // noinspection ES6ModulesDependencies
        layui.each(Object.keys(action_bar_east), function (idx, action_name) {
          var item = action_bar_east[action_name];
          _temp_container.push('<div class="layui-inline" title="' + item.title + '" lay-event="' + item.layEvent + '">' +
            '<i class="layui-icon ' + item.icon + '"></i>' + item.title + "</div>"
          );
          $(opts.container)
            .find(".layui-table-tool .layui-table-tool-self")
            .html(_temp_container.join(""));
        });

        opts.table.on("toolbar(" + opts.model + ")", function (obj) {
          //var checkStatus = table.checkStatus(obj.config.id);
          var othis = $(this),
            config = obj.config,
            openPanel = function openPanel(panel_opts) {
              var list = $(panel_opts.list),
                panel = $('<ul class="layui-table-tool-panel"></ul>');

              panel.html(list);
              othis.find(".layui-table-tool-panel")[0] || othis.append(panel);
              opts.form.render();

              panel.on("click", function (e) {
                layui.stope(e);
              });

              panel_opts.done && panel_opts.done(panel, list);
            };

          switch (obj.event) {
            case opts.model + "_columns":
              console.log('do some things columns');
              openPanel({
                list: (function () {
                  var lis = [];
                  layui.each(config.cols, function (idx1, item1) {
                    layui.each(item1, function (idx2, item2) {
                      if (item2.hide) {
                        last_hidden_fields.push(item2.field);
                      }
                      item2.field && "normal" === item2.type && lis.push('<li><input type="checkbox" name="' + item2.field + '" data-key="' + item2.key + '" data-parentkey="' + (item2.parentKey || "") + '" lay-skin="primary" ' + (item2.hide ? "" : "checked") + ' title="' + item2.title + '" lay-filter="header_cols"></li>');
                    });
                  });
                  return lis.join("");
                })(),
                panel_callback: function () {
                  if (!isArraysEqual(last_hidden_fields, cur_hidden_fields)) {
                    var best_fit_btn = othis.siblings(interpolate('[lay-event=%s]', [opts.model + '_fit'])),
                      last_fit_op = best_fit_btn.data(CONST_LAST_FIT_OP);
                    best_fit_click_handler.apply(null,
                      [othis, last_fit_op || opts.prefer_best_fit_op, config]);

                    layer.msg('Adjusting columns', {
                      title: 'Info',
                      icon: 16,
                      shade: 0.01,
                      time: 500
                    });
                  }
                  othis.parent().find('.open').removeClass('open');
                },
                done: function (panel, list, panel_opts) {
                  var last_hidden_fields = [];
                  // maintain_checkbox_status(panel);
                  opts.form.on("checkbox(header_cols)", function (obj) {
                    var cur_col_obj = $(obj.elem), checked = this.checked, key = cur_col_obj.data("key");
                    if (!checked) {
                      // remove rightest fixed bar in table-main
                      // var fix_column_r = $('.layui-table-body.layui-table-main .layui-table-col-special', '.layui-show');
                      var fix_column_r = $('.layui-table-fixed.layui-table-fixed-r .layui-table-col-special', '.layui-show');
                      if (!fix_column_r.hasClass('layui-hide')) {
                        fix_column_r.addClass('layui-hide');
                      }
                    }

                    layui.each(config.cols, function (i1, item1) {
                      layui.each(item1, function (i2, item2) {
                        if (item2.key === key) {
                          var hide = !checked;
                          item2.hide = hide;
                          if (hide !== opts.curTable.config.cols[i1][i2].hide) {
                            opts.curTable.config.cols[i1][i2].hide = hide;
                          }
                          $(opts.container).find('.layui-table-box *[data-key="' + config.index + "-" + key + '"]')[checked ? "removeClass" : "addClass"]("layui-hide");
                        }
                      });
                    });
                  });
                }
              });
              break;
            case opts.model + "_export":
              console.log('do some things export')
              openPanel({
                list: (function () {
                  return (
                    '<li data-type="csv">'+gettext('csv_export')+'</li>' +
                    // '<li data-type="pdf">'+gettext('pdf_export')+'</li>' +
                    '<li data-type="xls">'+gettext('xlsx_export')+'</li>' +
                    '<li data-type="txt">'+gettext('txt_export')+'</li>');
                })(),
                done: function done(panel, list/*, panel_opts*/) {
                  list.on("click", function () {
                    var export_type = $(this).data("type");
                    var stamp = (new Date()).getTime();
                    var csrf = $("input[name='csrfmiddlewaretoken']").val();
                    layui.use(["layer", "form"], function () {
                      var $ = layui.jquery,
                        layer = layui.layer,
                        form = layui.form,
                        curr_table_page = opts.curTable.config.page,
                        index = curr_table_page.curr,
                        limit = curr_table_page.limit,
                        count = curr_table_page.count;
                      var html =
                        '<div><form class="layui-form">' +
                        '<input type="hidden" name="csrfmiddlewaretoken" value="' + csrf + '"/>' +
                        '<input type="hidden" name="export_type" value="' + export_type + '"/>' +
                        '<input type="hidden" name="stamp" value="' + stamp + '"/>' +
                        '<div class="layui-form-item" pane="" style="padding-top:15px;">' +
                        // ' <div class="layui-input-block"><input type="checkbox" name="export_headers" ' +
                        // 'lay-skin="primary" title="Export with table header." checked=""></div>' +
                        "</div>" +
                        '<div class="layui-form-item" pane="">' + ' <div class="layui-input-block">' +
                        ' <input type="radio" name="page" value="' + index + '" title="Export current page data." checked="">' +
                        ' <input name="limit" value="' + limit + '" title="Export current page data." hidden>' +
                        ' <input name="page_size" value="' + count + '" title="Export current page data." hidden>' +
                        ' <input type="radio" name="page"  title="Export all data.">' +
                        "</div></div>" + "</form></div>";
                      layer.open({
                        type: 1,
                        zIndex: 998,
                        area: ['580px', '210px'],
                        title: gettext('next step'),
                        content: html,
                        btn: ["Submit", "Close"],
                        btnAlign: 'c',
                        success: function success(/*layero*/) {
                          form.render();
                        },
                        yes: function yes(/*index, layero*/) {
                          var export_headers = [];
                          var report_headers = opts.curTable.config.cols[0];
                          report_headers.forEach(function (item) {
                            if (item.hide === false) {
                              export_headers.push(item.field);
                            }
                          });
                          var input_page = $("form.layui-form input[name='page']");
                          var base_url = opts['export']
                          var table_config = get_layui_table_config(opts.model)

                          var url_params = {}
                          $.extend(url_params, get_layui_table_config(opts.model).where)

                          if (input_page[0].checked === true) {
                            $.extend(url_params, {
                              export_type: $("input[name='export_type']").val(),
                              page: table_config.page.curr,
                              limit: table_config.page.limit,
                              export_headers: export_headers.join(',')
                            });
                            // window.location.href = concatenate_url(base_url, url_params);
                            downloadFile(concatenate_url(base_url, url_params))
                          } else {
                            $.extend(url_params, {
                              export_type: $("input[name='export_type']").val(),
                              export_headers: export_headers.join(',')
                            });
                            downloadFile(concatenate_url(base_url, url_params))
                            // window.location.href = concatenate_url(base_url, url_params);
                          }
                        }
                      });
                    });
                  });
                }
              });
              break;
            default:
              console.log('do some things default')
              break;
          }
        });
        // 处理批量处理的操作
        if (opts.actions.length > 0) {
          var action_bind = function action_bind(elem) {
            $(elem).on('click', (function () {
              var action_index = $(this).attr("index");
              var group_index = $(this).attr("group");
              var group = opts.actions[group_index];
              var action = group.actions[action_index];
              var selected_data = opts.table.checkStatus(opts.curTable.config.id)
                .data;
              var op = $.extend({}, action, {objects: selected_data});
              var params = {
                elem: this,
                action: op
              };
              // noinspection JSUnresolvedVariable
              var need_select = op.batch_select;
              var need_one = op.unique;
              var _len = op.objects.length;
              // [constrain]: you need select at least one item
              if ((need_select || need_one) && _len === 0) {
                layer.msg('select nothing..', {time: 800,});
                return;
              }

              // [constrain]: this action can only select one item
              if (need_one && _len > 1) {
                layer.msg('select at most one item.', {time: 800,});
                return;
              }

              if (op["confirmation"]) {
                layui.use("layer", function () {
                  var layer = layui.layer;
                  var confirm_text = "";
                  if (need_select) {
                    confirm_text = String.format(
                      op["confirmation"],
                      _len,
                      _len > 1 ? gettext("item") : gettext("items")
                    );
                  } else {
                    confirm_text = op["confirmation"];
                  }
                  layer.confirm(
                    confirm_text,
                    {btn: ["ok", "cancel"], title: "Confirm"},
                    function (index, layero) {
                      action_post(op, index, layero);
                    }
                  );
                });
              } else {
                action_request(params);
                // action_request(op);
              }
            }));
          };

          opts.actions.forEach(function (action_set, idx_1) {
            //if (!opts.actions.hasOwnProperty(i)) {
            //  continue;
            //}
            var group = action_set["name"];
            var is_multi = action_set["multi"];
            var actions = action_set["actions"];
            var stamp = new Date().getTime();
            if (actions !== undefined && actions.length > 0) {
              if (!is_multi && actions.length === 1) {
                var _action = actions[0];
                var actionID = _action.name + stamp;
                var action_css = "layui-link-btn-" + _action.name;
                var action_elem =
                  '<li class="layui-nav-item"><a id="' + actionID + '" class="layui-btn layui-btn layui-btn-sm ' +
                  action_css + '" title="' + _action.description + '" group="' + idx_1 + '" index="0">' +
                  group + "</a></li>";
                $(opts.actionContainer)
                  .find(".layui-nav")
                  .append(action_elem);
                action_bind("#" + actionID);
              } else {
                var action_group =
                  '<li class="layui-nav-item"><a class="layui-btn layui-btn layui-btn-sm" href="javascript:void(0);">' + group + '<span class="layui-nav-more"></span></a><d1 class="layui-nav-child layui-anim layui-anim-upbit">';
                actions.forEach(function (_action, idx_2) {
                  var action_css = "layui-link-btn-" + _action.name;
                  var actionID = _action.name + stamp;
                  action_group += '<dd><a id ="' + actionID + '" class="layui-btn layui-btn layui-btn-sm ' +
                    action_css + '" title="' + _action.description + '" group="' + idx_1 + '" index="' +
                    idx_2 + '">' + _action["verbose_name"] + "</a></dd>";
                });
                action_group += "</d1></li>";
                $(opts.actionContainer)
                  .find(".layui-nav")
                  .append(action_group);
                actions.forEach(function (_action /*, j*/) {
                  var actionID = _action.name + stamp;
                  action_bind("#" + actionID);
                });
              }
            }
          });

          opts.element.render("nav");
        }
      };
      actionTemplate();
    };

    $.fn.reload_layui_data_grid = function (options) {
      var tab_item = $.fn.actions.defaults.current_tab_item,
        to_first = null, // jump back to first page
        hex_code = undefined;
      if (options) {
        to_first = options.to_first;
        hex_code = options.hex_code;
      }

      if (!to_first || typeof to_first !== "boolean") {
        to_first = true;
      }
      if (to_first && $('.layui-laypage-curr', tab_item).text() !== '1') {
        var $first_page = $('a:not([class])[data-page=1]', '.layui-show').get(0);
        $first_page && $first_page.dispatchEvent(new Event('click'));
      }
      var refresh_btn = $('.layui-laypage-refresh', tab_item).get(0);
      if (refresh_btn) {
        refresh_btn.dispatchEvent(new Event('click'));
      } else { // if we cannot find the refresh button, then call the reload method of table
        var model_name = null;
        if (!hex_code) { // if we didn't pass hex_code, then fetch it from html context
          hex_code = $('.layui-tab-title#zk-layui-tab-ul>li.layui-this').attr('lay-id');
        }
        if (hex_code !== undefined && typeof hex_code === 'string') {
          model_name = decodeHex(hex_code).replace(/\s|^\/|\/$/g, '').split('/')[1];
        }
        if (model_name && model_name.length > 0) {
          layui.use(['table', 'tablePlug'], function () {
            var table = layui.table, tablePlug = layui.tablePlug;
            tablePlug.smartReload.enable(true);
            table.reload(interpolate($.fn.actions.defaults.table_id_template, [model_name], false), {
              page: {curr: 1}
            });
          });
        }
      }
    };

    $.fn.actions.defaults = {
      editForm: "",
      current_tab_item: '.layui-tab-item.layui-show',
      table_id_template: 'id_grid_%s',
      actionContainer: "div.grid-toolbar",
      actionContainerUl: "div.grid-toolbar ul",
      addActionContainer: "div.grid-toolbar a.layui-link-btn-add",
      delActionContainer: "div.grid-toolbar a.layui-link-btn-del",
      refreshActionContainer: "div.grid-toolbar a.layui-link-btn-refresh",
      tipsFail: "Failed.",
      tipsSucc: "Successful!",
      $pageInput: null,
      $gridParent: null,
      pageButton: ".layui-laypage-btn",
      table: null,
      element: null,
      curTable: null,
      tableID: 0,
      actions: []
    };
  }
)(jQuery);
