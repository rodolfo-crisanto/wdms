(function () {
  // noinspection ES6ModulesDependencies
  $.fn.adminSite = function (options) {
    // noinspection ES6ModulesDependencies
    var opts = $.extend({}, $.fn.dataGrid.defaults, options);

    $("#left_menu .zk-switch-menu").on("click", function () {
      if ($(this).find("i.fa-outdent").length > 0) {
        $(this).find("i").removeClass("fa-outdent").addClass("fa-indent");
        $("#left_menu").addClass('layui-side-collapsed');
        $(".layui-layout-admin").addClass("zk-show-menu");
        $("li.layui-nav-itemed").removeClass('layui-nav-itemed');
        $(".layui-body").css('left', '60px');
      } else {
        $(this).find("i").removeClass("fa-indent").addClass("fa-outdent");
        $("#left_menu").removeClass('layui-side-collapsed');
        $('dd.layui-this').parents('li.layui-nav-item').addClass('layui-nav-itemed');
        $(".layui-layout-admin").removeClass("zk-show-menu");
        $(".layui-body").css('left', '200px');
      }
    });

    layui.config({
      base: opts.base_path,
      version: '1.5',
    }).extend({
      zkMenu: 'zkMenu',
      zkTab: 'zkTab',
      autoColumnWidth: 'autoColumnWidth.min',
      tablePlug: 'tablePlug',
    });

    layui.use(['element', 'layer', 'zkMenu', 'zkTab'], function () {
      var $ = layui.jquery, element = layui.element, menu = layui.zkMenu, tab = layui.zkTab;

      menu.render({data: opts.menu});
      tab.render({pin_tab_url: opts.pin_tab_url});


      var left_menu = $("#left_menu"),
        main_body = $("#main_body"),
        dash_board = $("#dashboard"),
        $group_list_str = '#grouplist>li';

      element.on("nav(nav-menu)", function (elem) {
        $.each($.fn.dashboard_timer || {}, function (key, val) {
          if (val !== null) {
            window.clearTimeout(val);
          }
        });

        var app_label = $(elem).attr('app');
        if (app_label === undefined) { // it is non-app
          left_menu.addClass("layui-hide");
          main_body.addClass("layui-hide");
          dash_board.removeClass("layui-hide"); // .remove();
          setTimeout(function () {
            $('.layui-nav-item .layui-this').removeClass('layui-this')
          }, 50);
          return true;
        }
        var $menu = $('#left_menu .zk-switch-menu');
        if ($menu.find("i.fa-indent").length > 0) {
          $menu.get(0).dispatchEvent(new Event('click'));
        }
        $($group_list_str + '.layui-nav-itemed' ).removeClass('layui-nav-itemed');
        $($group_list_str).hide();
        $($group_list_str + '[app="' + app_label + '"]').show();
        left_menu.removeClass("layui-hide");
        main_body.removeClass("layui-hide");
        dash_board.addClass("layui-hide"); // .remove();
        var tabs = $("#zk-layui-tab-ul li");
        $.each(tabs, function (i, t) {
          var layID = $(t).attr('lay-id');
          tab.tabDelete(layID, {
            record: false,
          });
        });
        // first we try to find the 'pined' menu item
        var module_sub_menu = $('#grouplist > li[app=' + app_label + ']'),
          first_sub_menu = module_sub_menu.first();
        var a_list = module_sub_menu.find('dl>dd>a[class~=pined-tab-item]'),
          amount = a_list.length;
        if (amount === 0) {
          first_sub_menu.addClass("layui-nav-itemed");
          first_sub_menu.next().addClass("layui-nav-itemed");
          first_sub_menu.find('dl > dd > a').first().click();
        } else {
          var interval = 1000;
          for (var i = 0; i < amount; i++) {
            $(a_list[i]).closest('li').addClass('layui-nav-itemed');
            setTimeout((function (elem, i) {
              return function () {
                var lay_idx = layer.msg(interpolate(gettext('loading pined-tab. %s / %s'), [i + 1, amount], false), {
                  title: 'Info',
                  icon: 16,
                  shade: 0.01,
                  time: 0,
                });
                $(elem).trigger('click');
                setTimeout(function () {
                  lay_idx && layer.close(lay_idx);
                }, 800);
              };
            })(a_list[i], i), interval * i);
          }
        }
      });

      element.on('nav(system-menu)', function (elem) {
        var ref = $(elem).attr('ref');
        var event = $(elem).attr('event');
        switch (event) {
          case "logout":
            layer.confirm('Are you sure to logout?', {icon: 3, title: 'Notice', btn: ['ok', 'cancel']}, function (index) {
              layer.close(index);
              // window.location = ref;
              Cookies.remove("auth-token");
              window.location = '/login/';
            });
            break;
          case "license":
            loadIndex = layer.load();
            $.ajax({
              url: '/license/license/'
              , type: "GET"
              , dataType: "html"
              , success: function (panel) {
                layer.closeAll();
                layer.open({
                  title: '<!--suppress HtmlUnknownTarget --><img src="/static/images/abroad_att/license-zk-logo.png" style="height:22px; margin-top: 10px;" alt="">'
                  , type: 1
                  , zIndex: 1001
                  , area: ['620px', '590px']
                  , content: panel
                });
              }
              , error: function () {
              }
            });
            break;
          case "changePassword":
            $.ajax({
              url: '/authentication/user/change_password/',
              type: 'GET',
              dataType: 'html',
              success: function (res) {
                layer.open({
                  type: 1,
                  title: 'Change Password',
                  zIndex: 1999,
                  area: ['auto', 'auto'],
                  content: res,
                  btn: ['save', 'cancel'],
                  yes: function (index, layero/*, callee*/) {
                    // var data_form = $(layero).find("#action-form, #data-form"),
                    //   data = data_form.serialize();
                    var data_form = $(layero).find("form.layui-form");
                    var post_data = {};
                    data_form = new FormData(data_form[0]);
                    data_form.forEach((value, key) => post_data[key] = value);
                    console.log(post_data)
                    $.ajax({
                      url: '/api/users/change_password/',
                      type: 'POST',
                      data: JSON.stringify(post_data),
                      dataType: "json",
                      success: function (response) {
                        if (response.code === 0) {
                          layer.close(index);
                          layer.alert(gettext('Auto logout after 3 seconds.'), {
                            title: 'Prompt',
                            time: 3000, //3s后自动关闭
                            btn: [gettext('Okay'),],
                            end: function () {
                              Cookies.remove("auth-token");
                              window.location = '/login/';
                            }
                          });
                        } else {
                          parse_msg_show_tips(layero, response.msg)
                        }
                      },
                      error: function (response) {
                        // parse_msg_show_tips(layero, response.responseText)
                        // console.log(response.responseText);
                        var res_msg = JSON.parse(response.responseText)
                        var display_msg = ''
                        for (var prop in res_msg) {
                          display_msg += '<p>' + prop + ": " + res_msg[prop] + '</p>'
                          $('form input[name="' + prop + '"]').addClass('layui-form-danger');
                          $('form select[name="' + prop + '"]').addClass('layui-form-danger');
                        }
                        layer.alert(display_msg, {'icon': 0, btn: ['ok'], title: 'message'})
                      }
                    });
                  },
                });
                layui.use('form', function () {
                  var form = layui.form;
                  form.render();
                });
              }
            });
            break;
          default:
            break;
        }
      });
    });

    function _pin_tab(lay_id, options) {
      var op = null, repr = null, module = null,
        done_callback = null, fail_callback = null;

      if (options) {
        op = options['op'];
        module = options['module'];
        repr = options['repr'];
        done_callback = options['done_callback'];
        fail_callback = options['fail_callback'];
      }

      if (!op) {
        op = 'append';
      }
      if (!repr) {
        repr = 'pin';
      }
      if (!module) {
        module = get_module_name();
      }

      $.ajax({
        type: 'POST',
        url: opts.pin_tab_url,
        data: {
          op: op,
          module: module,
          // pin_hex: lay_id,
          pin_url: decodeHex(lay_id),
        },
        dataType: 'json',
        success: function (res) {
          if (res.code === 0) {
            var _msg = res.msg;
            if (_msg === undefined) {
              _msg = gettext(repr + ' successfully');
            }
            layer.msg(gettext(_msg), {
              title: 'info',
              icon: 1,
              time: '1500',
              shade: 0.1,
              closeBtn: 1,
            });
            if (done_callback) {
              done_callback.call(this);
            }
          } else {
            parse_msg_show_tips(null, res.msg);
            if (fail_callback) {
              fail_callback.call(this);
            }
          }
        }, error: function () {
          console.log('# unpin failed', arguments);
          error_prompt(gettext(repr + ' failed'), 'auto');
        }
      });
    }

    $.fn.pin_tab = _pin_tab;

    $('#zk-layui-tab-ul').on('click', 'i.fa-thumb-tack.pined-tab', function () {
      var elem = $(this),
        tab_li = elem.closest('li[lay-id]'),
        lay_id = tab_li ? tab_li.attr('lay-id') : '';
      if (lay_id.length > 0) {
        layer.confirm(gettext('Are you sure to unpin?'), {
          title: 'Confirm',
          btn: [gettext('Sure'), gettext('Cancel')],
        }, function () {
          // noinspection ES6ModulesDependencies
          _pin_tab.apply(elem, [lay_id, {
            op: 'remove',
            repr: 'unpin',
            done_callback: function () {
              elem.remove();
              var tag_a = $(interpolate('.layui-nav-item.side-menu-group dl>dd>a[hex=%s]', [lay_id,], false));
              tag_a.removeClass('pined-tab-item');
              tag_a.find('i.fa-thumb-tack.pined-tab').remove();
            }
          }]);
        });
      }
    });
  };

  // noinspection ES6ModulesDependencies
  $.fn.adminSite.defaults = {
    base_path: 'static/extends',
    pined_tab: '<i class="fa fa-fw fa-thumb-tack pined-tab" aria-hidden="true"></i>',
  }
})(jQuery);
