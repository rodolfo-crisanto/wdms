// noinspection ES6ModulesDependencies
function titleCase(str, splitter) {
  if (splitter === undefined) {
    splitter = ' ';
  }
  return str.toLowerCase().split(splitter).map(function (word) {
    return (word.charAt(0).toUpperCase() + word.slice(1));
  }).join(' ');
}

function get_text_width(text, font) {
  if (!get_text_width.fakeEl) {
    get_text_width.fakeEl = $('<span>').hide().appendTo(document.body);
  }
  get_text_width.fakeEl.text(text || this.val() || this.text()).css('font', font /*|| this.css('font')*/
    || '14px Helvetica Neue,Helvetica,PingFang SC,Tahoma,Arial,sans-serif');
  return get_text_width.fakeEl.width();
}

function show_error_tips(msg, that, color, close_btn, more_tips) {
  if (color === undefined) {
    color = '#db5860';
  }
  if (close_btn === undefined) {
    close_btn = 1;
  }
  if (more_tips === undefined) {
    more_tips = true;
  }

  return layer.tips(msg, that, {
    tips: [2, color],
    time: 0,
    fixed: false,
    closeBtn: close_btn,
    tipsMore: more_tips
  });
}

function show_error_prompt(msg, extra_options) {
  var default_options = {
    icon: 2,
    time: 3000, // 3 seconds
    shade: 0.06,
    closeBtn: 2,
    title: gettext('Error')
  };
  // noinspection ES6ModulesDependencies
  var opt = $.extend({}, default_options, extra_options);

  layer.msg(msg, opt);
}

// noinspection JSUnusedGlobalSymbols
function show_multi_overlap_window(content, title) {
  if (title === undefined) {
    title = gettext('Error');
  }
  layer.open({
    type: 1,
    title: title,
    area: ['390px', '330px'],
    shade: 0,
    offset: [ // random location
      Math.random() * ($(window).height() - 300),
      Math.random() * ($(window).width() - 390)
    ],
    maxmin: true,
    content: content,
    btn: [gettext('close_all')],
    yes: function () {
      layer.closeAll();
    },
    zIndex: layer.zIndex, // key point 1
    success: function (layero) {
      layer.setTop(layero); // key point 2
    }
  });
}

function parse_msg_show_tips(where_form_is_at, msg) {
  if (msg === undefined) {
    return;
  }
  if (typeof msg === 'string') {
    if (msg.length === 0) {
      msg = gettext('Sorry, Something Wrong during handle your request.');
    }
    layer.msg(msg, {
      icon: 2,
      time: 0,
      closeBtn: 2,
      title: gettext('Error')
    });
    return;
  }
  var general_messages = [];
  // noinspection ES6ModulesDependencies
  $.each(msg, function (key, elem) {
    var messages = [];
    // noinspection ES6ModulesDependencies
    $.each(elem, function (index, detail) {
      messages.push(detail.message);
    });
    // the message is not belong to specific filed
    if ('__all__' === key) {
      general_messages.push(messages);
    } else {
      // console.log('#messages', messages);
      var that = where_form_is_at.find('.layui-form-item [id*=' + key + ']').closest('.layui-input-inline');
      // console.log(key, elem, that);
      if (that.length) {
        var _index = show_error_tips(messages.join('\n'), that);
        // handle click event
        that.on('click', function () {
          layer.close(_index);
        });
      } else {
        show_error_prompt(key + ': ' + messages);
      }
    }
  });
  if (general_messages.length > 0) {
    // show_multi_overlap_window(general_messages.join('\n'));
    show_error_prompt(general_messages.join('\n'));
  }
}

/**
 * function factory: make call-back function for submit
 * @param action_url: backend interface url for current submit actions
 * @param options
 *  - extra_field:
 *  @define: extra_field need to interject into form data
 *  @desc: **Note that it will use the `data` field passed by LayUI **
 *  - alternative_form: {type: selector}
 *  @define: use the `alternative_form` to replace the default `data` field(was mentioned above)
 * @returns {Function} callback function
 */
function submit_callback(action_url, options) {
  return function (data) {
    var extra_field = null,
      alternative_form = null;

    if (options && Object.keys(options).length > 0) {
      extra_field = options['extra_field'];
      alternative_form = options['alternative_form'];
    }

    var form_data = null;

    if (alternative_form === undefined || alternative_form == null) {
      form_data = data.field;
    } else {
      form_data = formObjectify(alternative_form);
    }

    form_data.obj_id = 1;

    if (extra_field && Object.keys(extra_field).length > 0) {
      // noinspection ES6ModulesDependencies
      $.each(extra_field, function (key, elem) {
        form_data[key] = elem;
      });
    }

    // noinspection ES6ModulesDependencies
    $.ajax({
      type: "POST",
      url: action_url,
      data: form_data,
      dataType: "json",
      success: function (res) {
        var msg, status;
        if (res.code === 0) {
          msg = gettext("save_successful");
          status = 1;
          layer.msg(msg, {
            icon: status,
            time: 2000,
            title: gettext('Info')
          });
        } else {
          var f = $('.layui-form', '.layui-show');
          parse_msg_show_tips(f, res.msg);
        }
      }
    });
  };
}

/**
 * internal error prompt(encapsulated)
 */
function error_prompt(msg, area) {
  if (area === undefined) {
    area = '600px';
  }
  show_error_prompt(msg, {
    title: 'Warning',
    shade: 0.3,
    time: 2000,
    closeBtn: 2,
    area: area
  });
}

/**
 *
 * @returns {*|jQuery}
 */
function get_module_name() {
  return $('.layui-nav[lay-filter=nav-menu] .layui-nav-item .layui-this>a').attr('app');
}

/**
 *
 * @param str
 * @returns {String|*|void|string}
 * // String.prototype.decodeHex = function() {
 */
function decodeHex(str) {
  try {
    return str.replace(/([0-9A-Fa-f]{2})/g, function () {
      return String.fromCharCode(parseInt(arguments[1], 16));
    });
  } catch (e) {
    return '';
  }
}

// noinspection JSUnusedGlobalSymbols
/**
 *
 * @param byteStream
 * @param utf8
 * @returns {string}
 * @constructor
 */
function encodeHex(byteStream, utf8) {
  if (utf8 === undefined || typeof utf8 !== 'boolean') {
    utf8 = false;
  }
  if (utf8) {
    byteStream = utf8.encode(byteStream);
  }
  var str = '';
  for (var i = 0; i < byteStream.length; i++) {
    str += byteStream[i].charCodeAt(0).toString(16);
  }
  return str;
}

if (typeof String.prototype.endsWith !== 'function') {
  String.prototype.endsWith = function (suffix) {
    // ensure the suffix is the last `suffix.length` characters
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
  };
}

if (typeof String.prototype.startsWith !== 'function') {
  String.prototype.startsWith = function (prefix) {
    return this.substring(0, prefix.length).indexOf(prefix, 0) !== -1;
  };
}

/**
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes#Polyfill
 */
if (!Array.prototype.includes) {
  Object.defineProperty(Array.prototype, 'includes', {
    value: function (searchElement, fromIndex) {

      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      // 1. Let O be ? ToObject(this value).
      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If len is 0, return false.
      if (len === 0) {
        return false;
      }

      // 4. Let n be ? ToInteger(fromIndex).
      //    (If fromIndex is undefined, this step produces the value 0.)
      var n = fromIndex | 0;

      // 5. If n ≥ 0, then
      //  a. Let k be n.
      // 6. Else n < 0,
      //  a. Let k be len + n.
      //  b. If k < 0, let k be 0.
      var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

      function sameValueZero(x, y) {
        return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
      }

      // 7. Repeat, while k < len
      while (k < len) {
        // a. Let elementK be the result of ? Get(O, ! ToString(k)).
        // b. If SameValueZero(searchElement, elementK) is true, return true.
        if (sameValueZero(o[k], searchElement)) {
          return true;
        }
        // c. Increase k by 1.
        k++;
      }

      // 8. Return false
      return false;
    }
  });
}

function intersection(o1, o2) {
  var list1 = [], list2 = [];

  if ($.isArray(o1)) {
    list1 = o1;
  } else if (o1) {
    list1 = Object.keys(o1);
  }
  if ($.isArray(o2)) {
    list2 = o2;
  } else if (o2) {
    list2 = Object.keys(o2);
  }


  return list1.concat(list2).sort().reduce(function (acc, cur, idx, src) {
    if (idx && src[idx - 1] === cur) {
      acc.push(cur);
    }
    return acc;
  }, []);
}

function _get_reverse_version_params(where) {
  var normal_pat = '_p0_',
    reverse_pat = '_p1_',
    result = [];
  $.each(where, function (param/*, value*/) {
    // reverse of parameter
    var rp = null;
    if (param.startsWith(normal_pat)) {
      rp = param.replace(new RegExp('^' + normal_pat), reverse_pat);
    } else if (param.startsWith(reverse_pat)) {
      rp = param.replace(new RegExp('^' + reverse_pat), normal_pat);
    }
    if (rp) {
      result.push(rp);
    }

  });
  return result;
}

function drop_last_request_parameters(w1, w2) {
  var to_keep = intersection(w1, w2),
    filter_pat = /^_p\d_/;

  $.each(Object.keys(w1), function (idx, elem) {
    // only delete those filter parameters that not includes in this request
    if (filter_pat.test(elem) && $.inArray(elem, to_keep) === -1) {
      delete w1[elem];
    }
  });
  return w1;
}

/**
 * remove mutual exclude parameter in the w1 according w2's configures
 * @param w1
 * @param w2
 */
function drop_mutual_exclude_parameters(w1, w2) {
  var reverse_params = _get_reverse_version_params(w2),
    to_remove = intersection(w1, reverse_params);
  $.each(to_remove, function () {
    delete w1[this];
  });
  return w1;
}


(function ($) {
  var proxy = $.fn.serializeArray;
  $.fn.serializeArray = function () {
    var inputs = this.find(':disabled');
    inputs.prop('disabled', false);
    var serialized = proxy.apply(this, arguments);
    inputs.prop('disabled', true);
    return serialized;
  };
})(jQuery);


/**
 * make html element
 * @param tag
 * @param attributes
 * @returns {HTMLElement}
 * @private
 */
function make_dom_element(tag, attributes) {
  var elem = document.createElement(tag);
  for (var attribute in attributes) {
    if (attributes.hasOwnProperty(attribute)) {
      if (attribute === "innerText") {
        elem["innerText"] = attributes[attribute];
      } else {
        elem.setAttribute(attribute, attributes[attribute]);
      }
    }
  }
  return elem;
}

$.fn.make_dom_element = make_dom_element;


function get_layui_table_property(category, model_name) {
  return layui.table.thisTable[category]['id_grid_' + model_name];
}

get_layui_table_instance = function (model_name) {
  return get_layui_table_property('that', model_name);
};

get_layui_table_config = function (model_name) {
  return get_layui_table_property('config', model_name);
};

/**
 *
 * @param url: {str}, required
 * @param iterator: {Object, FormData}, required
 * @param exclude: {str, Array} optional, used in iterate the iterator for exclude some unwanted key-value pair
 */
function concatenate_url(url, iterator, exclude) {
  var original_url = url,
    junction = '?',
    request_args = [];

  if (exclude === undefined) {
    exclude = [];
  } else if (typeof exclude === 'string') {
    exclude = [exclude];
  }

  if (original_url.includes('?')) {
    junction = '&';
  }
  var push_function = function (option) {
    var key = option.key,
      value = option.value;
    if (exclude.indexOf(key) === -1) {
      request_args.push(key + '=' + value);
    }
  };

  var push_function_wrapper = function (reverse) {
    if (reverse === undefined || typeof reverse !== 'boolean') {
      reverse = false;
    }
    return function (arg1, arg2) {
      if (reverse) {
        arg2 = [arg1, arg1 = arg2][0];
      }
      push_function({
        'key': arg1,
        'value': arg2
      });
    };
  };

  if (iterator instanceof FormData) {
    iterator.forEach(push_function_wrapper(true));
  } else {
    $.each(iterator, push_function_wrapper());
  }

  return original_url + junction + request_args.join('&');
}


/**
 *
 * @param url: {str}, required
 * func: downloadFile from url, and add Authorization Header to Request
  */
function downloadFile(url) {
    var req = new XMLHttpRequest();
    req.open("GET", url, true);
    req.setRequestHeader("Authorization", Cookies.get("auth-token"));
    req.setRequestHeader('content-type', 'application/json');
    req.responseType = "blob";
    req.onload = function (event) {
        var blob = req.response;
        var fileName = req.getResponseHeader("Content-Disposition").split('filename=')[1] //if you have the fileName header available
        var link=document.createElement('a');
        link.href=window.URL.createObjectURL(blob);
        link.download=fileName;
        link.click();
    };
    req.send();
}