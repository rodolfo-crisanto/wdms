#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

echo "stoping docker-compose"
docker-compose stop
echo "stopped docker-compose"
