(function () {
  var range_select_inited = false;
  // noinspection ES6ModulesDependencies
  $.fn.dataGrid = function (options, url) {
    // noinspection ES6ModulesDependencies
    var opts = $.extend({}, $.fn.dataGrid.defaults, options);

    function get_available_area_of_table(row_num, tolerant_offset) {
      if (tolerant_offset === undefined || type(tolerant_offset) !== 'number') {
        tolerant_offset = 10;
      }

      return ($.fn.dataGrid.defaults.CONST_SINGLE_LAY_TABLE_ROW_HEIGHT * row_num) +
             $.fn.dataGrid.defaults.CONST_LAY_TABLE_OTHER_PARTS_HEIGHT + Math.floor(Math.random() * tolerant_offset) + 1;
    }

    layui.use(['table', 'form', 'element', 'laydate', 'autoColumnWidth', 'tablePlug'], function () {
      var $ = layui.jquery, table = layui.table, form = layui.form, element = layui.element,
        laydate = layui.laydate, tablePlug = layui.tablePlug; // autoColumnWidth = layui.autoColumnWidth;
      $(opts.fluid_container + " div.search-form input.layui-datetime-range").each(function () {
        laydate.render({elem: this, type: 'datetime', range: '~', lang: 'en'});
      });
      form.render();

      var headers = {'content-type':'application/json'};
      var row_limit = opts.grid_opts.limit;

      // deep_copy: true
      $.extend(true, opts.grid_opts, {
        url: opts.url,
        headers: headers,
        id: opts.table_id,
        elem: opts.table_elem,
        toolbar: opts.toolbar_id,
        defaultToolbar: [],
        smartReloadModel: true,
        autoSort: false,  // disable front-end sorting
        cellMinWidth: 80,
        height: 'full-200',
        // height: 460, //single row: 23px
        loading: true,
        page: {
          layout: ['refresh', 'limit', 'prev', 'page', 'next', 'count', 'skip'],
          curr: 1 //Start from first page
        }, text: {
          none: gettext('none_data')
        },
        limits: [5, 10, 20, 30, 60, 90],
        page_size: 30,
        where: {}
      });

      var grid_table = table.render(opts.grid_opts); //获取数据并渲染

      $(opts.fluid_container + " div.grid-toolbar").actions({
        container: opts.fluid_container,
        actionContainer: opts.fluid_container + " div.grid-toolbar",
        model: opts.model,
        table: table,
        tableID: opts.table_id,
        form: form,
        element: element,
        curTable: grid_table,
        gridOpts: opts.grid_opts,
        actions: opts.actions,
        export: opts.export_url,
        dimension: opts.dimension,
        callback: opts.callback['actions'],
      });

      function _default_callback() {
        // _default_callback 用于刷新页面
        $.fn.do_filter({
          model: opts.model,
          table: table,
          table_id: opts.table_id
        });
      }

      tablePlug.smartReload.enable(true);
      $(".layui-nav-bar").addClass('layui-hide');
      table.on('checkbox(' + opts.model + ')', function (obj) {
        console.log(obj.data); //选中行的相关数据
      });

      /**
       * @desc: server-end ordering
       * @param:
       * - obj.field: 当前排序的字段名
       * - obj.type: 当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
       * - this: 当前排序的 th 对象
       *
       * 注：sort 是字段排序事件名， filter 是table原始容器的属性 lay-filter="对应的值"
       */
      table.on('sort(' + opts.model + ')', function (obj) {
        var sort = {}, msg;
        if (obj.type == null) {
          sort.field = undefined;
          sort.type = undefined;
        } else {
          // sort = JSON.parse(JSON.stringify(obj));
          sort = $.extend(sort, obj);
        }

        // console.log(sort.field, sort.type)
        let ordering_value = sort.field
        if(sort.type === 'desc') {
          ordering_value = '-' + ordering_value
        }

        table.reload(opts.table_id, {
          initSort: obj, //记录初始排序，如果不设的话，将无法标记表头的排序状态。
          where: { //请求参数
            ordering: ordering_value //排序方式及字段
          }
        });

        if (sort.type) {
          msg = gettext('Sorting by') + ' ' + this[0].textContent + ' ' + sort.type;
        } else {
          msg = gettext('Sorting by') + ' ' + gettext('default field.');
        }
        layer.msg(msg, {
          time: 1000, closeBtn: 2
        });
      });

      table.on('tool(' + opts.model + ')', function (obj) {
        var data = obj.data;

        if (opts.all_perms.change) {
          function send_edit_ajax(index, layero) {
            var valid_form = window['before_submit_' + "id_grid_" + opts.model];
            if (valid_form && $.isFunction(valid_form) && !valid_form()) {
              return;
            }

            var layui_form = $(layero).find("form.layui-form");
            var data_form = new FormData(layui_form[0]);
            var post_data = {};
            data_form.forEach((value, key) => post_data[key] = value);
            console.log(post_data);
            $.ajax({
              url: opts.url + data['id'] + '/',
              type: "PUT",
              data: JSON.stringify(post_data),
              dataType: "json",
              headers: {'content-type':'application/json',},
              success: function(response, status, xhr){
                console.log(response);
                layer.close(index);
                layer.closeAll('loading');
                _default_callback.apply(opts);
                layer.msg('Success', {time: 1000, icon: 1});
                //需要刷新页面
              },
              error: function (response) {
                layer.closeAll('loading');
                var res_msg = JSON.parse(response.responseText)
                var display_msg = ''
                for (var prop in res_msg) {
                  display_msg += '<p>' + prop + ": " + res_msg[prop] + '</p>'
                  $('form input[name="' + prop + '"]').addClass('layui-form-danger');
                  $('form select[name="' + prop + '"]').addClass('layui-form-danger');
                }
                layer.alert(display_msg, {'icon': 0, btn: ['ok'], title: 'message'})
                console.log(response.responseText);
              }
            });

            $(layero).find("div.layui-form-button a.layui-btn-cancel").click(function () {
              layer.close(index);
            });
          }

          if (obj.event === 'editPopUp' || obj.event === 'edit') {
            $.ajax({
              url: opts.edit_url,
              type: 'GET',
              dataType: 'html',
              success: function (res) {
                layer.open({
                  type: 1,
                  zIndex: 998,
                  //skin: 'layui-layer-rim',
                  area: [opts.dimension.width, opts.dimension.height],
                  content: res,
                  btn: ['save', 'cancel'],
                  yes: send_edit_ajax,
                  end: function () {
                    layer.closeAll('tips');
                  },
                  title: gettext("Operation")
                });
                init_edit_data(opts.url + data['id'] + '/')
              }
            });
          }
        } else {
          if (obj.event === 'editPopUp' || obj.event === 'edit') {
            layer.msg('Permission denied', {time: 500});
          }
        }

        //监听工具条
        //此处监听为监听obj.event,如编辑和删除操作，不监听批量操作等
        if (data === undefined) {
          data = obj.data;
        }
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        // var tr = obj.tr; //获得当前行 tr 的DOM对象
        console.log(layEvent);

        if (obj.event === 'get_cmd') {
            $.ajax({
              url: '/device/iclock/cmd_result/',
              type: 'GET',
              dataType: 'html',
              success: function (res) {
                layer.open({
                  type: 1,
                  zIndex: 998,
                  area: [750, 400],
                  content: res,
                  title: 'Cmd Result'
                });
                  cmd_grid_opts = {
                    url: '/api/iclocks/cmd_result/?sn=' + data['sn'],
                    elem: '#cmd_result',
                    smartReloadModel: true,
                    autoSort: false,  // disable front-end sorting
                    height: 'full-200',
                    height: 330, //single row: 23px
                    loading: true,
                    cols: [[
                      {field:'content', width:'60%', title: 'Content'}
                      ,{field:'return_time', width:'25%', title: 'Return Time', sort: true}
                      ,{field:'return_value', width:'15%', title: 'Result', templet: '#return_valueTpl'}
                    ]],
                    page: {
                      layout: ['refresh', 'limit', 'prev', 'page', 'next', 'count', 'skip'],
                      curr: 1 //Start from first page
                    }, 
                    text: {
                      none: 'None'
                    },
                    limits: [5, 10, 20, 30, 60, 90],
                    page_size: 30,
                    where: {}
                  }
                  table.render(cmd_grid_opts);
              }
            });
          }

        if (layEvent === 'detail') { //查看
          if (opts.all_perms.view) {

          }
        }
        if (layEvent === 'del') { //删除
          if (opts.all_perms.delete) {
            layer.confirm('Are you sure?',
              {title: 'Confirm', icon: 3, btn: ['Yep', 'Nope']},
              function (index) {
                layer.close(index);
                //向服务端发送删除指令
                $.ajax({
                  url: opts.url + data['id'] + '/',
                  type: 'DELETE',
                  dataType: 'json',
                  success: function (response, status, xhr) {
                    layer.close(index);
                    _default_callback.apply(opts);
                    layer.msg("Successful", {time: 1000, icon: 1, title: 'Info'});
                  },
                  error: function (response) {
                    parse_msg_show_tips(layer, response.responseText);
                  },
                });
              });
          } else {
            layer.msg('Permission denied', {time: 500});
          }
        }
      }); // end interactEvent listener

      // $('#' + opts.model + '-search_btn').on('click', function () {
      //   var start = $("input[name='" + opts.model + "-start']").val();
      //   var end = $("input[name='" + opts.model + "-end']").val();
      //   console.log(start, end)
      //   // table.reload(tb.config.id, {
      //   //   where: {
      //   //     end_time_gte: start,
      //   //     start_time_lte: end,
      //   //     category: $('#Category_arv').val()||null,
      //   //     audit_status: $('#" + opts.model + "-aStatus').val()||null
      //   //   }
      //   // });
      //   table.reload('id_grid_' + opts.model, {
      //     page: {
      //       curr: 1 //重新从第 1 页开始
      //     }, where: {
      //       company_id: start,
      //       company_name: end,
      //     }
      //   });
      // });

      // 此处作用未知
      var active_action = {
        search: function (btn_id) {
          var _where = {};
          var all_search_input = $('.layui-input[id^=search_]');
          var btn_2_input = {};
          for (var i = 0, n = all_search_input.length; i < n; i++) {
            btn_2_input[all_search_input[i]['id'] + '_btn'] = all_search_input[i];
          }

          var field = btn_2_input[btn_id].name;
          _where[field] = $(btn_2_input[btn_id]).val();
          //执行重载

          table.reload('id_grid_' + opts.model, {
            page: {
              curr: 1 //重新从第 1 页开始
            }, where: {
              key: JSON.stringify(_where)
            }
          });
        }
      };
      $('.layui-btn[id^=search_][id$=_btn]').on('click', function () {
        var type = $(this).data('type');
        //console.log('click_search: ', type, $(this).data('type'), $(this).get(0).id);
        active_action[type] ? active_action[type].apply(this, [$(this)[0]['id']]) : '';
      });
    }); // end use table

    // 作用未知
    $(document).on("click", "td div.layui-table-cell", "td div.laytable-cell-checkbox div.layui-form-checkbox", function (e) {
      e.stopPropagation();
    });
  };

  // noinspection ES6ModulesDependencies
  $.fn.dataGrid.defaults = {
    // fluid_container: "",
    // table_id: "",
    // table_elem: "",
    // toolbar_id: "",
    // grid_opts: {},
    // actions: {},
    // all_perms: {},
    // dimension: {},
  };
})(jQuery);
