var type = (function (global) {
  var cache = {};
  return function (obj) {
    var key;
    return (
      obj === null
      ? 'null' // null
      : obj === global
        ? 'global' // window in browser or global in nodejs
        : (key = typeof obj) !== 'object'
          ? key // basic: string, boolean, number, undefined, function
          : obj.nodeType
            ? 'object' // DOM element
            // cached. date, regexp, error, object, array, math
            : cache[key = Object.prototype.toString.call(obj)] ||
              // get XXXX from [object XXXX], and cache  it
              (cache[key] = key.replace(/(?:\[object )(.*)(?:])/, '$1').toLowerCase())
    );
  };
}(this));

var menuClick = function (url) {
  $.ajax({
    url: url,
    type: "GET",
    dataType: 'html',
    success: function (callback) {
      $("#id_main_body").html(callback);
    }
  });
};

/* JavaScript equivalent to python Format */
if (!String.format) {
  String.format = function (format) {
    var args = Array.prototype.slice.call(arguments, 1);
    return format.replace(/{(\d+)}/g, function (match, number) {
      return typeof args[number] !== 'undefined'
        ? args[number]
        : match
        ;
    });
  };
}

/**
 * @summary Remove duplicate element
 * @author Boyce
 * @description
 * >>> arr = Array.prototype.slice.call('12414111', '')
 * --> Array(8) [ "1", "2", "4", "1", "4", "1", "1", "1" ]
 * >>> arr.toUnique()
 * --> Array(3) [ "1", "2", "4" ]
 * @returns unique array
 */
if (!Array.prototype.toUnique) {
  Array.prototype.toUnique = function () {
    var _arr = this.concat();
    for (var i = 0, n = _arr.length; i < n; ++i) {
      for (var j = i + 1; j < _arr.length; ++j) {
        if (_arr[i] === _arr[j]) {
          // say: you remove this guy from queue,
          // and next loop you will continue progress from current index again
          _arr.splice(j--, 1);
        }
      }
    }
    return _arr;
  };
}

/**
 * @author Boyce
 * @date 20180907
 * @summary 类似 python % 的字符格式化方法
 * @desc 仅支持 %s, 因为 String 最常见, 非 str, toString() 一步转换
 *
 * @param fmt 格式字符所组成的串
 * @param obj {Array|Object} 如果是用 named 具名参数，则 obj instanceof Object 成立，否则 obj instanceof Array 成立
 * @param named {Boolean} 决定是否使用 named 具名参数
 * @returns {String|*|void|string} 返回`格式字符`已被`对应的实际字符`替换后的字符串
 *
 * @example
 * ``` javascirpt
 * // 未使用具名参数
 * result = interpolate('%s, %s', ['hello', 'boyce'])
 * hello, boyce
 * // 使用具名参数
 * result = interpolate('%(msg)s, %(name)s', {'msg':'goodbye', 'name':'boyce'}, true)
 * goodbye, boyce
 * ```
 */
function interpolate(fmt, obj, named) {
  var _sentry = ''; //
  if (named) {
    if (obj instanceof Object) {
      return fmt.replace(/%\(\w+\)s/g, function (match) {
        var res = _sentry;
        try {
          res = String(obj[match.slice(2, -2)])
        } catch (e) {
          console.log('# WARNING:', e);
        }
        return res;
      });
    }
  } else if (obj instanceof Array) {
    return fmt.replace(/%s/g, function (/*match*/) {
      var res = _sentry;
      try {
        res = String(obj.shift())
      } catch (e) {
        console.log('# WARNING: There is not enough arguments to shift.');
      }
      return res;
    });
  }
  return fmt;
}

/**
 * @author Boyce
 * @date 20180907
 * @summary 和 interpolate 类似, 差异点在于模拟的是 python 的 format 方法
 *
 * @param fmt
 * @param obj
 * @param named
 * @returns {String|*|void|string}
 */
function interject(fmt, obj, named) {
  if (named) {
    return fmt.replace(/[{]\w+?[}]/g, function (match) {
      return String(obj[match.slice(1, -1)])
    });
  } else {
    return fmt.replace(/{}/g, function (/*match*/) {
      return String(obj.shift())
    });
  }
}

/**
 * @author Boyce
 * @date 20180914
 * @param that -- Tips 所依附的 DOM or jQuery 选择器
 */
function tipsPrompt(that) {
  // console.log('call `tips_prompt`', this, arguments);
  var $that = $(that),
    box_target = $that.data('boxTarget'),
    text = $(box_target).text();

  layer.tips(text, that, {
    tips: [2, '#393d49'],
    time: 3000,
    closeBtn: 2,
  });

  /* // traditional tips layer
  layer.open({
    content: text,
    time: 3000,
    btn: [gettext('Okay'),],
    title: gettext('Tips'),
  });
  */
}

/**
 * @author Boyce
 * @desc check whether support html5 storage or not
 * @returns {boolean}
 */
function supports_html5_storage() {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
  } catch (e) {
    return false;
  }
}

/**
 * @author Boyce
 * @param str: the original string to encode
 * @returns {string} standard base64 encoded string
 * @example:
 * b64EncodeUnicode('✓ à la mode'); // "4pyTIMOgIGxhIG1vZGU="
 * b64EncodeUnicode('\n'); // "Cg=="
 */
function b64EncodeUnicode(str) {
  // first we use encodeURIComponent to get percent-encoded UTF-8,
  // then we convert the percent encodings into raw bytes which
  // can be fed into `btoa`.
  return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
    function toSolidBytes(match, p1) {
      return String.fromCharCode('0x' + p1);
    }));
}

/**
 * @author Boyce
 * @param str: standard base64 encoded string
 * @returns {string} the original string
 * @example:
 * b64DecodeUnicode('4pyTIMOgIGxhIG1vZGU='); // "✓ à la mode"
 * b64DecodeUnicode('Cg=='); // "\n"
 */
function b64DecodeUnicode(str) {
  // Going backwards: from bytestream, to percent-encoding, to original string.
  return decodeURIComponent(atob(str).split('').map(function (c) {
    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));
}

/**
 * let a form become a JSON
 * @param form: form selector or form obj
 */
function formObjectify(form) {
  if (typeof form === "string") {
    form = $(form);
  }
  var result = {};
  if (form instanceof Object && form.length > 0) {
    // form.serializeArray().map(function (x) {
    //   result[x.name] = x.value;
    // });
    // return result;
    return form.serializeArray().reduce(function (o, x) {
      o[x.name] = x.value;
      return o;
    }, result);
  } else {
    return result;
  }
}


function getCookie(c_name)
{
if (document.cookie.length>0)
  {
  c_start=document.cookie.indexOf(c_name + "=")
  if (c_start!=-1)
    {
    c_start=c_start + c_name.length+1
    c_end=document.cookie.indexOf(";",c_start)
    if (c_end==-1) c_end=document.cookie.length
    return unescape(document.cookie.substring(c_start,c_end))
    }
  }
return ""
}

// function setCookie(c_name,value,expiredays,path)
// {
// var exdate=new Date()
// exdate.setDate(exdate.getDate()+expiredays)
// document.cookie=c_name+ "=" +escape(value)+
// ((path==null) ? ";path=" + path : "/")+
// ((expiredays==null) ? "" : ";expires="+exdate.toGMTString())
// }
function setCookie(name, value, expires, path, domain, secure){
  document.cookie = name + "=" + escape(value) +
    ((expires) ? "; expires=" + expires : "") +
    ((path) ? "; path=" + path : "") +
    ((domain) ? "; domain=" + domain : "") +
    ((secure) ? "; secure" : "");
}


function delCookie(name)
{
var exp = new Date();
exp.setTime(exp.getTime() - 1);
var cval=getCookie(name);
if(cval!=null)
document.cookie= name + "="+cval+";expires="+exp.toGMTString();
}