/**
 * Created by Arvin on 2018/9/10.
 * Modified by Boyce on 2018.10.20
 */
layui.define(['element'], function (exports) {
  var $ = layui.jquery, element = layui.element,
    // process_bar_selector = '.layui-progress[lay-filter=pace]',
    // LIMITED_FREQUENCY = 500,
    // last_click_time = null,
    AUTO_DISAPPEAR_SECOND = 2,
    is_last_tab_loaded = true,
    last_click_module_item = null,
    last_click_menu_item = null,
    $group_list_str = '#grouplist>li',
    $tab_ul_str = '#zk-layui-tab-ul',
    $tab_title_str = '.layui-tab-title' + $tab_ul_str,
    $all_tab_li_str = $tab_title_str + ' li',
    $tab_list_template_str = $tab_title_str + ' li[lay-id=%s]',
    $related_menu_item_str = '.layui-nav-item.side-menu-group dl>dd>a[hex=%s]',
    // tab_open_history = {}, //  every app each have individual open history
    tab_close_history = {}, //  every app each have individual close history
    ZKTab = function () {
      this.config = {
        leftMenu: 'admin-nav',
        tabFilter: "zkTab",
        tabOC: "#zk-layui-tab",
        tabUl: $tab_ul_str,
        pin_html: $.fn.adminSite.defaults.pined_tab,
        limiter_popup: false,
      }
    };


  function click_tab_related_menu_item(lay_id) {
    $(interpolate($related_menu_item_str, [lay_id], false)).trigger('click');
  }

  /**
   *
   * @param before
   * @returns {boolean}
   */
  function check_tab_status(before) {
    if (before === undefined) {
      before = true;
    }

    var tab_cnt = $($all_tab_li_str).length;

    if (before === true) {
      if (tab_cnt === 1) {
        layer.msg(gettext('The last tab cannot be closed'), {
          title: 'Warning',
          icon: 0,
          time: 2000,
          shade: 0.3,
          closeBtn: 2,
          anim: 6,
        });
        return false
      }
    }

    if (tab_cnt > 1) {
      return true;
    } else {
      !before && $($all_tab_li_str).find('i.layui-icon.layui-tab-close').addClass('layui-hide');
      return false;
    }
  }

  // function set_process_bar(filter, percent, hide) {
  //   element.progress(filter, percent + '%');
  //   if (percent === 0) {
  //     $(process_bar_selector).show();
  //   }
  //   if (hide) {
  //     setTimeout(function () {
  //         $(process_bar_selector).fadeOut("slow");
  //       }, 800
  //     )
  //   }
  // }

  function callback_of_frequency_limiter(_id, _module) {
    if (last_click_menu_item) {
      if (last_click_menu_item !== _id) {
        $('a[hex=' + _id + ']').closest('dd').removeClass('layui-this');
      }
      $('a[hex=' + last_click_menu_item + ']').closest('dd').addClass('layui-this');
    }
    if (last_click_module_item) {
      if (last_click_module_item !== _module) {
        $('.menu_module>a[app=' + _module + ']').closest('.menu_module').removeClass('layui-this');
      }
      $('.menu_module>a[app=' + last_click_module_item + ']').closest('.menu_module').addClass('layui-this');
      $($group_list_str).hide();
      $($group_list_str + '[app="' + last_click_module_item + '"]').show();
    }
    // ZKTab.prototype.append.apply(_tab, [elem,]) // let user enter load the content of tab anyway
  }

  ZKTab.prototype.append = function (elem) {
    var _tab = this,
      _module = get_module_name(),
      _id = $(elem).attr('hex'),
      url = $(elem).attr('link');

    if (!url) {
      return;
    }
    layer.closeAll('tips'); // necessary to remove tips which belong to other tabs
    // console.time();
    if ($(_tab.config.tabUl + " > li[lay-id=" + _id + "]").length === 0) {
      // if (last_click_time && ((new Date().getTime()) - last_click_time  < LIMITED_FREQUENCY)) {
      if (_tab.config.limiter_popup && !is_last_tab_loaded) {
        layer.msg(gettext('Request too frequent! please wait the content of current tab load completely.'), {
          title: 'Prompt',
          icon: 0,
          area: '450px',
          time: AUTO_DISAPPEAR_SECOND * 1000,
          shade: [0.5, '#000'],
          anim: 1,
          closeBtn: 2,
        }, callback_of_frequency_limiter);
        return;
      } else {
        !is_last_tab_loaded && callback_of_frequency_limiter();
      }
      // set_process_bar('pace', 0);
      var title = $(elem).html(),
        record_delete_option = {
          module: _module,
          record: true,
        }, forgive_delete_option = {
          module: _module,
          record: false,
        };
      is_last_tab_loaded = false;
      $.get({
        // xhr: function () {
        //   var xhr = new window.XMLHttpRequest();
        //   //Download progress
        //   xhr.addEventListener("progress", function (evt) {
        //     if (evt.lengthComputable) {
        //       var percentComplete = (evt.loaded / evt.total) * 100;
        //       set_process_bar('pace', percentComplete);
        //     }
        //   }, false);
        //   return xhr;
        // },
        url: url,
        dataType: 'html',
        success: function (response) {
          try {
            element.tabAdd(_tab.config.tabFilter, {
              title: title,
              content: response, //支持传入html
              id: _id
            });
          } catch(e) {
            console.warn('# tabAdd: ', e);
            if (e instanceof SyntaxError) {
              error_prompt('Oops! Cannot open new tab. Because the content of tab contains illegal characters.');
            } else {
              error_prompt('Oops! The requested tab is unavailable currently.');
            }
            return;
          }
          // remove the original event handler
          $(interpolate($tab_list_template_str, [_id,], false) + ' .layui-tab-close').off('click').on('click', function () {
            _tab.tabDelete(_id, record_delete_option);
          });
          _tab.tabChange(_id);
          is_last_tab_loaded = true;
          // last_click_time = new Date().getTime();
          last_click_menu_item = _id;
          last_click_module_item = _module;

          var context_menu = new BootstrapMenu(interpolate($tab_list_template_str, [_id,], false), {
            fetchElementData: function ($elem) {
              return $elem.attr('lay-id');
            },
            actions: [
              {
                name: 'Reload Tab',
                onClick: function (_id) {
                  _tab.tabDelete(_id, forgive_delete_option);
                  click_tab_related_menu_item(_id);
                }
              // }, {
              //   name: 'Pin Tab',
              //   onClick: function (_id) {
              //     $.fn.pin_tab(_id, {
              //       op: 'append',
              //       repr: 'pin',
              //       done_callback: function () {
              //         $(interpolate($tab_list_template_str, [_id,], false)).find('span').before($(_tab.config.pin_html));
              //         var related_menu_item = $(interpolate($related_menu_item_str, [_id,], false));
              //         related_menu_item.find('span').before($(_tab.config.pin_html));
              //         related_menu_item.addClass('pined-tab-item');
              //       }
              //     });
              //   }
              }, {
                name: 'Re-open last close tab',
                onClick: function (/*_id*/) {
                  var id_of_stored_tabs = tab_close_history[_module];
                  if (id_of_stored_tabs === undefined || id_of_stored_tabs.length === 0) {
                    // alert user: there is nothing in close history
                    error_prompt(gettext('There is nothing in close history'), 'auto');
                  } else {
                    var lay_id = id_of_stored_tabs.pop();
                    click_tab_related_menu_item(lay_id);
                    tab_close_history[_module] = id_of_stored_tabs;
                  }
                }
              }, {
                name: '---',
                onClick: function () {
                }
              }, {
                name: '🡣 Close Current Tab',
                onClick: function (_id) {
                  if (check_tab_status(true)) {
                    _tab.tabDelete(_id, record_delete_option);
                  }
                  check_tab_status(false);
                }
              }, {
                name: '🡠 Close tab on the left',
                onClick: function (_id) {
                  var all_li = $($all_tab_li_str);
                  for (var _n in all_li) {
                    if (all_li.hasOwnProperty(_n) && /\d+/.test(_n)) {
                      var elem = $(all_li[_n]),
                        lay_id = elem.attr('lay-id');
                      if (lay_id === _id) {
                        break;
                      }
                      _tab.tabDelete(lay_id, record_delete_option);
                    }
                  }
                  check_tab_status(false);
                }
              }, {
                name: '🡢 Close tab on the right',
                onClick: function (_id) {

                  var all_li = $($all_tab_li_str),
                    _len = all_li.length;
                  for (var _i = _len - 1; _i >= 0; _i--) {
                    var elem = $(all_li[_i]),
                      lay_id = elem.attr('lay-id');
                    if (lay_id === _id) {
                      break;
                    }
                    _tab.tabDelete(lay_id, record_delete_option);
                  }
                  check_tab_status(false);
                }
              }, {
                name: '⮠⮡ Close all tab but this',
                onClick: function () {
                  var _siblings = context_menu.$openTarget.siblings();
                  $.each(_siblings, function (idx, elem) {
                    var lay_id = $(elem).attr('lay-id');
                    _tab.tabDelete(lay_id, record_delete_option);
                  });
                  check_tab_status(false);
                }
              }]
          });
        }, error: function () {
          is_last_tab_loaded = true;
          // last_click_time = new Date().getTime();
          last_click_menu_item = _id;
          last_click_module_item = _module;
          console.warn('# Error: Append tab: ', arguments);
        }
      });
    }
    else {
      _tab.tabChange(_id);
      // jump to page 1 if current page is not equal '1'
      $.fn.reload_layui_data_grid({
        to_first: true,
        hex_code: _id,
      });
      last_click_menu_item = _id;
    }
    // console.timeEnd();
  };

  ZKTab.prototype.tabChange = function (tab_id) {
    var _tab = this;
    $(_tab.config.tabUl + " li:first i.layui-tab-close").hide();
    element.tabChange(this.config.tabFilter, tab_id);
  };

  ZKTab.prototype.tabDelete = function (tab_id, options) {
    var record = null, module = null;
    if (options) {
      record = options.record;
      module = options.module;
    }
    if (!record || typeof record !== 'boolean') {
      record = true;
    }
    if (record) {
      if (!module) {
        // get module immediately
        module = get_module_name();
      }

      // add closed tab id to tab_close_history
      var previous_result = tab_close_history[module];
      if (previous_result === undefined) {
        previous_result = [];
      } else {
        do {
          var loc = previous_result.indexOf(tab_id);
          if (loc !== -1) {
            previous_result.splice(loc, 1);
          }
        } while (loc !== -1);
      }
      previous_result.push(tab_id);
      tab_close_history[module] = previous_result;
    }
    element.tabDelete(this.config.tabFilter, tab_id);
  };
  ZKTab.prototype.render = function (opts) {
    var _tab = this; // only in he entrance, _tab equal to ZKTable instance
    $.extend(true, _tab.config, opts);
    //Tab render
    _tab.bindTab();
    //Add Tab
    element.on('nav(' + _tab.config.leftMenu + ')', function (elem) {
      $(_tab.config.tabOC).removeClass("layui-hide");
      _tab.append(elem);
    })
  };
  ZKTab.prototype.menuSelected = function (tabItem) {
    // console.log(tabItem)
  };
  ZKTab.prototype.bindTab = function () {
    var _tab = this;
    element.on('tab(' + _tab.config.tabFilter + ')', function () {
      var tabItem = $(this).attr('lay-id');
      //selected item css
      _tab.menuSelected(tabItem)
    })
  };
  var zkTab = new ZKTab();
  exports('zkTab', zkTab)
});
